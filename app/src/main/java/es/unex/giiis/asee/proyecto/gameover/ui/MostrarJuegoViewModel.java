package es.unex.giiis.asee.proyecto.gameover.ui;

import androidx.lifecycle.ViewModel;

import es.unex.giiis.asee.proyecto.gameover.data.Repository;
import es.unex.giiis.asee.proyecto.gameover.data.model.Videojuego;

/**
 * {@link ViewModel} for {@link MainActivity}
 */
class MostrarJuegoViewModel extends ViewModel {

    private final Repository mRepository;

    public MostrarJuegoViewModel(Repository repository) {
        mRepository = repository;
    }

    public void actualizarVideojuego(Videojuego videojuego){
        mRepository.updateVideojuego(videojuego);
    }

    public void deleteVideojuego(Videojuego videojuego){
        mRepository.deleteVideojuego(videojuego);
    }


}
