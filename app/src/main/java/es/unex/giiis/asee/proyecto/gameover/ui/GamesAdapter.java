package es.unex.giiis.asee.proyecto.gameover.ui;


import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.List;

import es.unex.giiis.asee.proyecto.gameover.R;
import es.unex.giiis.asee.proyecto.gameover.data.model.Videojuego;


public class GamesAdapter extends RecyclerView.Adapter<GamesAdapter.MyViewHolder>{

    private List<Videojuego> mDataset = new ArrayList<Videojuego>();

    public interface OnListInteractionListener{
        public void onListInteraction(String url);
    }

    public OnListInteractionListener mListener;

    private ItemClickListener onItemClickListener;


    public interface ItemClickListener {
        void onItemClick(Videojuego videojuego);
    }

    public void setItemClickListener(ItemClickListener clickListener) {
        onItemClickListener = clickListener;
    }


    // Provide a reference to the views for each data item
    // Complex data items may need more than one view per item, and
    // you provide access to all the views for a data item in a view holder
    public static class MyViewHolder extends RecyclerView.ViewHolder {
        // each data item is just a string in this case
        public TextView mTextView;
        public TextView mDateView;
        public ImageView mImageView;
        public TextView mVentas;
        public View mView;

        public Videojuego mItem;

        public MyViewHolder(View v) {
            super(v);
            mView=v;
            mTextView = v.findViewById(R.id.idNombre);
            mDateView = v.findViewById(R.id.idInfo);
            mImageView = v.findViewById(R.id.idImagen);
            mVentas = v.findViewById(R.id.ventas);

        }

        public void bind(final Videojuego videojuego, final ItemClickListener onItemClickListener) {
            itemView.setOnClickListener(new View.OnClickListener() {
                @Override public void onClick(View v) {
                    onItemClickListener.onItemClick(videojuego);
                }
            });

        }
    }

    // Provide a suitable constructor (depends on the kind of dataset)
    public GamesAdapter(List<Videojuego> myDataset, OnListInteractionListener listener) {
        mDataset = myDataset;
        mListener = listener;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent,
                                                        int viewType) {
        // create a new view
        // Create new views (invoked by the layout manager)
        View v = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_list, parent, false);

        return new MyViewHolder(v);
    }

    // Replace the contents of a view (invoked by the layout manager)
    @Override
    public void onBindViewHolder(final MyViewHolder holder, int position) {
        holder.bind(mDataset.get(position),onItemClickListener);
        //holder.mItem = mDataset.get(position);
        //Log.i("Dato",holder.mItem.getTitle());
        holder.mTextView.setText(mDataset.get(position).getTitle());
        if(mDataset.get(position).getSalePrice().compareTo("0.00")==0){
            holder.mDateView.setText("GRATIS");
        }else{
            holder.mDateView.setText(mDataset.get(position).getSalePrice() + "€");
        }
        if(mDataset.get(position).getThumb() != null) {
            Picasso.get().load(mDataset.get(position).getThumb()).into(holder.mImageView);
        }else{
            Picasso.get().load("https://http2.mlstatic.com/storage/mshops-appearance-api/images/15/254304515/logo-2020060212005277900.png").into(holder.mImageView);
        }
        if(!mDataset.get(position).isEsUsuario()){
            int n = Integer.parseInt(mDataset.get(position).getSavings().substring(0,mDataset.get(position).getSavings().indexOf('.')));
            String ventas = Integer.toString(n) + " millones";
            holder.mVentas.setText(ventas);
        }else{
            String ventas = mDataset.get(position).getSavings() +  " millones";
            holder.mVentas.setText(ventas);
        }

    }

    // Return the size of your dataset (invoked by the layout manager)
    @Override
    public int getItemCount() {
        return mDataset.size();
    }

    public List<Videojuego> getmDataset() {
        return mDataset;
    }

    public void swap(List<Videojuego> dataset){
        mDataset = dataset;
        notifyDataSetChanged();
    }

    public void clear(){
        mDataset.clear();
        notifyDataSetChanged();

    }
}