package es.unex.giiis.asee.proyecto.gameover.data.Network;

import android.util.Log;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;

import java.util.List;

import es.unex.giiis.asee.proyecto.gameover.AppExecutors;
import es.unex.giiis.asee.proyecto.gameover.data.model.Videojuego;

public class GameNetworkDataSource {
    private static final String LOG_TAG = GameNetworkDataSource.class.getSimpleName();
    private static GameNetworkDataSource sInstance;
    private GamesNetworkLoaderRunnable runnable;

    // LiveData storing the latest downloaded weather forecasts
    private final MutableLiveData<List<Videojuego>> mDownloadedVideojuego;

    private GameNetworkDataSource() {
        mDownloadedVideojuego = new MutableLiveData<>();
        runnable = new GamesNetworkLoaderRunnable(listJuegos -> mDownloadedVideojuego.postValue(listJuegos));
        AppExecutors.getInstance().networkIO().execute(runnable);
    }

    public synchronized static GameNetworkDataSource getInstance() {
        Log.d(LOG_TAG, "Getting the network data source");
        if (sInstance == null) {
            sInstance = new GameNetworkDataSource();
            Log.d(LOG_TAG, "Made new network data source");
        }
        return sInstance;
    }

    public LiveData<List<Videojuego>> getVideojuegosActuales() {
        return mDownloadedVideojuego;
    }

}
